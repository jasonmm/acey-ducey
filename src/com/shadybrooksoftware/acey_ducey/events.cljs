(ns com.shadybrooksoftware.acey-ducey.events
  (:require
    [re-frame.core :refer [reg-event-fx reg-event-db reg-fx]]
    [com.shadybrooksoftware.acey-ducey.game :as g]
    [com.shadybrooksoftware.acey-ducey.utils :as utils]))

(reg-fx
  :set-route
  (fn [{:keys [url]}]
    (utils/set-hash! url)))

(reg-event-fx
  ::initialize-app
  (fn [_ _]
    {:db {}}))

(reg-event-fx
  ::route-dispatch
  (fn [_ [_ route-info]]
    (let [route-id (:handler route-info)]
      (case route-id
        :home {:dispatch [::set-active-panel :home-panel]}
        :game-board {:dispatch [::set-active-panel :game-board]}
        {:dispatch [::set-active-panel :invalid-route]}))))

(reg-event-db
  ::set-active-panel
  (fn [db [_ active-panel]]
    (assoc db :active-panel active-panel)))

(reg-event-db
  ::new-game
  (fn [db _]
    (assoc db :game (g/new)
              :wager-amount 0)))

(reg-event-db
  ::end-game
  (fn [db _]
    (dissoc db :game)))

(reg-event-db
  ::wager-change
  (fn [db [_ wager-amount]]
    (cond-> (assoc db :wager-amount (parse-long wager-amount)
                      :invalid-wager? false)
      (< (get-in db [:game :money])
         wager-amount)
      (assoc :invalid-wager? true))))

(reg-event-fx
  ::place-wager
  (fn [{:keys [db]} _]
    (let [game (g/place-wager (:game db) (:wager-amount db))]
      (cond-> {:db (assoc db :game game)}
        (= :push (:turn-phase game))
        (assoc :fx [[:dispatch [::reveal]]])))))

(reg-event-db
  ::next-turn
  (fn [db _]
    (let [game (g/deal (:game db))]
      (-> db
          (assoc :wager-amount 0
                 :game game)))))

(reg-event-fx
  ::double-wager
  (fn [{:keys [db]}]
    {:db (update-in db [:game :wager-amount] * 2)
     :fx [[:dispatch [::reveal]]]}))

(reg-event-db
  ::reveal
  (fn [db _]
    (assoc db :game (g/reveal (:game db)))))
