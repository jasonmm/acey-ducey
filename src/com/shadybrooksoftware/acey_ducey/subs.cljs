(ns com.shadybrooksoftware.acey-ducey.subs
  (:require
    [re-frame.core :refer [reg-sub]]
    [com.shadybrooksoftware.acey-ducey.game :as g]))

(reg-sub ::active-panel (fn [db _] (:active-panel db)))
(reg-sub ::game (fn [db _] (:game db)))
(reg-sub ::wager-amount (fn [db _] (:wager-amount db)))
(reg-sub ::invalid-wager? (fn [db _] (boolean (:invalid-wager? db))))

(reg-sub
  ::hidden-card-visible?
  :<- [::game]
  (fn [game]
    (:hidden-card-visible? game)))

(reg-sub
  ::wager-won?
  :<- [::game]
  (fn [game]
    (g/win? game)))

(reg-sub
  ::last-event-was-push?
  :<- [::game]
  (fn [game]
    (-> game :events last :push?)))

(reg-sub
  ::spread
  :<- [::game]
  (fn [game]
    (g/card-spread game)))

(reg-sub
  ::payout
  :<- [::spread]
  (fn [spread]
    (g/payout-multiplier spread)))

