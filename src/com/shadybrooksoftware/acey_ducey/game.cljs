(ns com.shadybrooksoftware.acey-ducey.game)

(defrecord GameState
  [id
   deck
   events
   high-card
   hidden-card
   low-card
   money
   turn-phase
   wager-amount])


(defn shuffled-deck
  "Returns a collection for the game to use as the starting deck."
  []
  (shuffle
    (for [suit [:spades :hearts :diamonds :clubs]
          face [2 3 4 5 6 7 8 9 10 :j :q :k]]
      (vector suit face)))
  #_ [[:spades 2] [:hearts :k] [:hearts 5]
      [:spades 2] [:hearts 2] [:clubs 2]
      [:spades 2] [:hearts 2] [:clubs 5]
      [:spades 2] [:hearts 3] [:clubs 6]])

(defn suit
  "Given a card from the game's deck return that card's suit."
  [card]
  (first card))

(defn face
  "Given a card from the game's deck return that card's face value."
  [card]
  (second card))

(defn face-value
  "Given a card, `[suit face]`, return the numeric value of the face. This is
  used to sort cards."
  [card]
  (let [f (face card)]
    (if (pos-int? f)
      f
      (get {:j 11 :q 12 :k 13 :a 14} f))))

(defn shuffle-if-necessary
  [game]
  (if (< (count (:deck game)) 3)
    (let [deck (shuffled-deck)]
      (-> game
          (assoc :deck deck)
          (update :events conj {:event :shuffle-deck
                                :deck  deck})))
    game))

(defn- perform-deal
  [game]
  (let [[low-card high-card] (->> game :deck (take 2) (sort-by face-value))
        hidden-card (nth (:deck game) 2)
        deck        (drop 3 (:deck game))]
    (-> game
        (assoc :low-card low-card
               :high-card high-card
               :hidden-card hidden-card
               :deck deck)
        (update :events conj {:event       :deal
                              :low-card    low-card
                              :high-card   high-card
                              :hidden-card hidden-card}))))

(defn deal
  "Return a `GameState` after dealing the top cards from the deck."
  [game]
  (-> game
      (assoc :wager-amount 0
             :turn-phase :wager)
      shuffle-if-necessary
      perform-deal))

(defn new
  "Return a `GameState` record for the start of a new game. This function
  creates the game and performs the first deal."
  []
  (let [deck (shuffled-deck)]
    (deal (map->GameState {:id          (random-uuid)
                           :money       100
                           :deck        deck
                           :low-card    nil
                           :high-card   nil
                           :hidden-card nil
                           :turn-phase  :wager
                           :events      [{:deck  deck
                                          :event :game-created}]}))))

(defn win?
  "Return whether the current turn is a win for the player."
  [game]
  (let [low-card-value    (-> game :low-card face-value)
        high-card-value   (-> game :high-card face-value)
        hidden-card-value (-> game :hidden-card face-value)]
    (or (= low-card-value high-card-value hidden-card-value)
        (< (-> game :low-card face-value)
           (-> game :hidden-card face-value)
           (-> game :high-card face-value)))))

(defn push?
  "Return whether the current turn is a push for the player."
  [game]
  (or (zero? (:wager-amount game))
      (and (= (-> game :low-card face-value)
              (-> game :high-card face-value))
           (not= (-> game :high-card face-value)
                 (-> game :hidden-card face-value)))))

(defn card-spread
  "Return the number of values between the high and low cards (not including
  the high and low cards). For example, the spread between 5 and 8 is 2."
  [game]
  (dec (- (face-value (:high-card game))
          (face-value (:low-card game)))))

(def payouts
  "A map of spread to payout. For example, a 2 card spread pays 4-to-1."
  {-1 11
   1  5
   2  4
   3  2})

(defn payout-multiplier
  "Given a spread return the payout, the default payout is 1-to-1."
  [spread]
  (or (get payouts spread) 1))

(defn update-money
  "Update the player's `money` in the `game` based on whether the hidden card
  is between the visible cards."
  [game]
  (let [wager-amount (:wager-amount game)
        turn-won?    (win? game)
        turn-pushed? (push? game)]
    (update game :money (fn [money]
                          (if turn-won?
                            (let [payout-amount (-> game
                                                    card-spread
                                                    payout-multiplier
                                                    (* wager-amount))]
                              (+ money payout-amount))
                            (if turn-pushed?
                              money
                              (- money wager-amount)))))))

(defn push
  "Return the `GameState` when the turn is a push."
  [game reason]
  (-> game
      (assoc :turn-phase :push)
      (update :events conj {:event  :push
                            :reason reason})))

(defn- add-wager-placed-event
  [game wager-amount]
  (update game :events conj {:event  :wager-placed
                             :amount wager-amount}))

(defn place-wager
  "Return the `GameState` after the player has placed their wager."
  [game wager-amount]
  (if (zero? wager-amount)
    (push game :zero-wager)
    (case (card-spread game)
      -1 (-> game
             (assoc :turn-phase :equal-value
                    :wager-amount wager-amount)
             (add-wager-placed-event wager-amount))
      0 (push game :consecutive-spread)
      (-> game
          (assoc :turn-phase :double-wager?
                 :wager-amount wager-amount)
          (add-wager-placed-event wager-amount)))))

(defn reveal
  "Return the `GameState` after the middle (hidden) card has been revealed."
  [game]
  (-> game
      (assoc :turn-phase :reveal)
      update-money
      (update :events conj {:event        :turn-ends
                            :wager-amount (:wager-amount game)
                            :win?         (win? game)
                            :push?        (push? game)})))
