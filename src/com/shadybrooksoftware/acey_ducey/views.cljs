(ns com.shadybrooksoftware.acey-ducey.views
  (:require
    [re-frame.core :as re]
    [com.shadybrooksoftware.acey-ducey.subs :as subs]
    [com.shadybrooksoftware.acey-ducey.game :as g]
    [com.shadybrooksoftware.acey-ducey.events :as events]
    [com.shadybrooksoftware.acey-ducey.utils :refer [<sub] :as u]
    [clojure.string :as str]))

(defn- container
  [body]
  [:div.flex.flex-col.items-center.w-72.md:w-96.m-6.pb-10.mx-auto body])

(defn- primary-button
  [opts label]
  [:div.flex.justify-center.p-3
   [:button.py-3.px-6.bg-sky-300.text-black.rounded-xl.hover:bg-sky-900.hover:text-white.duration-300
    opts
    label]])

(defn- neutral-button
  [opts label]
  [:button.py-1.px-6.border.border-slate-500.text-black.rounded-xl.hover:bg-neutral-800.hover:text-white.duration-300
   opts
   label])

(defn- action-button
  [opts label]
  [:button.py-2.px-3.bg-sky-300.text-black.rounded-xl.hover:bg-sky-900.hover:text-white.duration-300.text-sm.disabled:opacity-25.disabled:bg-stone-200
   opts
   label])

(defn- card
  ([suit face]
   (card suit face false))
  ([suit face facedown?]
   (let [face    (if (keyword? face) (name face) face)
         classes (cond-> [(str "card-" (name suit))
                          (str "card-" face)]
                   facedown? (conj "card-facedown"))]
     [:div.card {:class (str/join " " classes)} [:span]])))

(defn action-row
  [body]
  [:div.flex.flex-row.justify-between.items-center.my-2.space-x-4 body])

(defn wager-row
  "Display a text field for entering the wager amount and a 'Place Wager' button."
  []
  (let [invalid-wager? (<sub [::subs/invalid-wager?])]
    [:<>
     [:div.flex.flex-row.items-center
      {:class "w-1/2"}
      [:p "$"]
      [:input.placeholder-italic.border-b.border-stone-200.p-2.focus:ring-0.text-end.w-full
       {:class     (str/join " "
                             (cond-> []
                               invalid-wager? (conj "border-red-500" "text-red-500")))
        :type      :text
        :value     (<sub [::subs/wager-amount])
        :on-change #(re/dispatch [::events/wager-change (u/target-value %)])}]]
     [action-button
      {:disabled invalid-wager?
       :on-click #(re/dispatch [::events/place-wager])}
      "Place Wager"]]))

(defn double-wager-row
  "The component asking the player if they would like to double their wager."
  []
  [:<>
   [:p "Double the wager?"]
   [:button.py-1.px-6.bg-emerald-300.text-black.rounded-xl.hover:bg-emerald-500.hover:text-white.duration-300
    {:on-click #(re/dispatch [::events/double-wager])}
    "Yes"]
   [:button.py-1.px-6.bg-neutral-500.text-white.rounded-xl.hover:bg-neutral-700.duration-300
    {:on-click #(re/dispatch [::events/reveal])}
    "No"]])

(defn win-message
  []
  [:div.text-green-500.font-bold.text-center "You Won!!"])

(defn loss-message
  []
  [:div.text-red-500.italic.text-center "You Lost!"])

(defn pass-message
  []
  [:div.text-black.italic.text-center.uppercase "Pass"])

(defn automatic-push-message
  []
  [:div.text-black.italic.text-center "Spread is 0. Push."])

(defn next-turn-button
  []
  [action-button {:on-click #(re/dispatch [::events/next-turn])} "Next Turn"])

(defn turn-results
  "Display the results of the last wager."
  []
  (let [win?         (<sub [::subs/wager-won?])
        wager-amount (<sub [::subs/wager-amount])]
    [:<>
     [:div (if (pos-int? wager-amount)
             (if win? [win-message] [loss-message])
             [pass-message])]
     [next-turn-button]]))

(defn reveal-row
  "Display the results of the turn and the ability for the player to continue
  or quit."
  []
  (let [win?  (<sub [::subs/wager-won?])
        push? (<sub [::subs/last-event-was-push?])]
    [:<>
     (if win?
       [win-message]
       (if push?
         [:div.text-black.italic.text-center "Push."]
         [loss-message]))
     [:div.flex.flex-col.space-y-3
      [action-button
       {:on-click #(re/dispatch [::events/next-turn])}
       "Next Turn"]
      [neutral-button
       {:on-click #(re/dispatch [::events/end-game])}
       "Quit"]]]))

(defn equal-value
  [wager-amount]
  [:<>
   [:div.text-center
    [:p "Cards are equal."]
    [:p "If the middle card is also equal then you will receive a payout of $" (* (get g/payouts -1) wager-amount) ". Otherwise the turn is a push."]]
   [action-button
    {:on-click #(re/dispatch [::events/reveal])}
    "Continue"]])

(defn automatic-push-row
  []
  [:<>
   [:div [automatic-push-message]]
   [next-turn-button]])

(defn spread-row
  []
  (let [spread     (<sub [::subs/spread])
        payout     (<sub [::subs/payout])
        spread-msg (str spread " card spread.")
        payout-msg (str "Payout is " payout "-to-1.")]
    [:div.flex.flex-row.justify-center.italic
     (str
       (when (pos-int? spread) spread-msg)
       payout-msg)]))

(defn cards-row
  [game]
  (let [wager-phase?  (= :wager (:turn-phase game))
        reveal-phase? (= :reveal (:turn-phase game))]
    [:div.flex.flex-row.justify-around.my-3
     [card
      (-> game :low-card g/suit)
      (-> game :low-card g/face)
      wager-phase?]
     [card
      (-> game :hidden-card g/suit)
      (-> game :hidden-card g/face)
      (not reveal-phase?)]
     [card
      (-> game :high-card g/suit)
      (-> game :high-card g/face)
      wager-phase?]]))

(defn game-board
  [game]
  [:div.w-full
   [:p "You have $" (:money game)]

   [cards-row game]

   (case (:turn-phase game)
     :wager [action-row [wager-row]]
     :double-wager? [action-row [double-wager-row]]
     :reveal [action-row [reveal-row]]
     :equal-value [action-row [equal-value (:wager-amount game)]])])

(defn home-menu
  []
  [:div.flex.flex-col.items-center
   [:a.text-sm.text-stone-300.mb-5.cursor-pointer
    {:href "https://en.wikipedia.org/wiki/Yablon#Yablon_gameplay"}
    "Yablon Rules"]
   [primary-button
    {:on-click #(re/dispatch [::events/new-game])}
    "New Game"]
   [:p.mt-5.border.border-slate-500.rounded-lg.p-2.text-center.bg-emerald-50.text-stone-500
    "Three cards are dealt. You are wagering that the middle card falls numerically between the other two cards."]])

(defn home-panel []
  (let [game (<sub [::subs/game])]
    [container
     [:<>
      [:h1.text-5xl.mb-3 "Acey-Ducey"]
      (if-not game
        [home-menu]
        [game-board game])]]))

(defn main-panel
  []
  (case (<sub [::subs/active-panel])
    :home-panel [home-panel]
    [:div "Invalid panel"]))
